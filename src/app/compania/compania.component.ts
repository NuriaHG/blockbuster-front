import { Component, OnInit } from '@angular/core';
import { Compania } from './compania';

@Component({
  selector: 'app-compania',
  templateUrl: './compania.component.html',
  styleUrls: ['./compania.component.css']
})


export class CompaniaComponent implements OnInit  {

  companias : Compania [] = [
    
    {idCompania:'1', cif: '42215815H', nombreCompania:'ABC'}, 
    {idCompania:'2', cif: '43215818M', nombreCompania:'DEF' },
    {idCompania:'3', cif: '41526815L', nombreCompania:'GHI'}, 
    {idCompania:'4', cif: '43215818M', nombreCompania:'JKL' }];
  
 
    showId: boolean=true;

    Constructor(){}
    ngOnInit() {

    }

    switchId(): void { 
      this.showId = !this.showId;
    }
}


