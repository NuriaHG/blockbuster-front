import { Juego } from './juego';

export const JUEGOS: Juego[] = [
    {idJuego: 1, titulo:'Maniac Manson', fechaLanzamiento:'05-10-1987', precio: 5, pegi:12, categoria:'AVENTURA GRÁFICA'},
    {idJuego: 2, titulo:'Doom', fechaLanzamiento:'01-12-1993',precio: 10, pegi:15, categoria:'SHOOTER'},
    {idJuego: 3, titulo:'Counter-Strike', fechaLanzamiento:'09-11-2000', precio: 15, pegi:16, categoria:'SHOOTER'},
    {idJuego: 4, titulo:'Fortnite', fechaLanzamiento:'07-21-2017', precio: 20, pegi:16, categoria:'AVENTURA MMO/MOBA?'}];