import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
   
  juegos: Juego[]; 

  showId: boolean = true;

  constructor(private juegoService: JuegoService) {
  }
      
    
  ngOnInit() {
      this.juegoService.getJuegos().subscribe(
        juegos => this.juegos = juegos
      );

  }

  switchId(): void { 
      this.showId = !this.showId;
  }
}

