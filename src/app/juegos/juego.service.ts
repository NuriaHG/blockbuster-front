import { Injectable } from '@angular/core';
import { Observable, of, from, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Juego } from './juego';
import { JUEGOS } from './juegos.json';
import { AlertService } from '../_alert/alert.service';



@Injectable ()
export class JuegoService {

  urlServer: string = 'http://localhost:8090/';

  constructor( private http: HttpClient, private alertService: AlertService) { }

  getJuegos(): Observable<Juego[]> {
   
    return this.http.get<Juego[]>(`${this.urlServer}juegos`).pipe (
      catchError(error => {
        console.error(`getJuegos error: "${error.message}"`);
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`)
        return throwError(error);
        
      })

    );
     
  }

} 