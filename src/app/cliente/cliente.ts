export class Cliente {

    idCliente: number;
    nombre: string;
    dni: string;
    fechaNacimiento: string;
    correo: string;
}
