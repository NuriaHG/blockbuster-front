import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit {

  clientes : Cliente [] = [
    {idCliente: 1, nombre:'Cristina', dni: '42215816Y', fechaNacimiento:'03-12-1989', correo:'cristinapadron@hotmail.com'},
    {idCliente: 2, nombre:'Marianne', dni: '43215816L', fechaNacimiento:'11-30-1971', correo:'mariannenaranjo@gmail.com' },
    {idCliente: 3, nombre:'Elena',dni: '45321212P', fechaNacimiento:'12-06-1989', correo:'elena_casty@gmail.com' },
    {idCliente: 4, nombre:'Vanesa',dni: '45372562K', fechaNacimiento:'11/26/1989', correo:'vanesa.santana@gmail.com'}];

    showId: boolean=true;

    Constructor(){}
    ngOnInit() {
      

    }

    switchId(): void { 
      this.showId = !this.showId;
    }
}
