import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { JuegosComponent } from './juegos/juegos.component';
import { ClienteComponent } from './cliente/cliente.component';
import { CompaniaComponent} from './compania/compania.component';
import { TiendaComponent } from './tienda/tienda.component';
import { StockComponent } from './stock/stock.component';
import { JuegoService } from './juegos/juego.service';

import { HttpClientModule } from '@angular/common/http';
import { AlertModule } from './_alert';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './header/header.component';
import { FormComponent } from './juegos/form.component';

const ROUTES: Routes = [
  {path:'',redirectTo: '/juegos', pathMatch:'full'},
  {path:'juegos', component:JuegosComponent},
  {path:'juegos/form', component:FormComponent},



];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    JuegosComponent,
    ClienteComponent,
    CompaniaComponent,
    TiendaComponent,
    StockComponent,
    HeaderComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    
    HttpClientModule,
    AlertModule,
    RouterModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [JuegoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
