import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';


@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css']
})
export class TiendaComponent implements OnInit {

  tiendas: Tienda [] = [

    { idTienda:'1', nombre:'Game Store', direccion:'c/García de Toledo, nº56'},
    { idTienda:'2', nombre:'Videojuegos Tenerife', direccion:'c/Andrés de la Torre, nº15'},
    { idTienda:'3', nombre:'Videoclub Nuevo Atogo', direccion:'c/Hermanos Méndez, nº98'}];

    showId: boolean=true;

    Constructor(){}
    ngOnInit() {

    }

    switchId(): void { 
      this.showId = !this.showId;
    }
}

