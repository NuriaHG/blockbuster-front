import { Component, OnInit } from '@angular/core';
import { Stock} from './stock';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})

export class StockComponent implements OnInit {

  stocks: Stock [] =  [

    {idStock:'1', numRef: '12345678', nombreJuego: 'Maniac Mansion', estadoStock:'Vendido'},
    {idStock:'2', numRef: '876543210', nombreJuego: 'Doom', estadoStock:'Alquilado'},
    {idStock:'3', numRef: '189451236', nombreJuego: 'Counter-Strike', estadoStock:'Alquilado'},
    {idStock:'4', numRef: '531267890', nombreJuego: 'Fortnite', estadoStock:'Vendido'}];


 showId: boolean=true;

    Constructor(){}
    ngOnInit() {

    }

    switchId(): void { 
      this.showId = !this.showId;
    }
}

